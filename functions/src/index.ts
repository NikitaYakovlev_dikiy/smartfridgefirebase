import * as Fridge from './bdRaw/fridge';
import * as User from './bdRaw/user';
import * as Product from './bdRaw/product';
import * as Message from './bdRaw/message';
import * as Auth from './auth/checkAuth'


export const auth = Auth.sampleRequest
export const fridge = new Fridge.FridgeTable().fun()
export const user = new User.UserTable().fun()
export const product = new Product.ProductTable().updatedfun()
export const message = new Message.MessageTable().fun()