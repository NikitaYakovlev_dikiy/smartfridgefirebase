import * as config from '../config'

export function getUser(uid: any): any | null {
    config.admin.auth().getUser(uid)
        .then(function (userRecord) {
            return userRecord
        })
        .catch(function (error) {
            return null
        })
}