import * as userAuth from './userAuth'
import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'

export const sampleRequest = functions.https.onRequest((request, response) => {
    
    console.log(request)
    console.log(request.headers.authorization)

    const user: admin.auth.UserRecord | null = userAuth.getUser(request.headers.authorization)

    console.log(user)

    if (user === null) {
        return response.sendStatus(400)
    } else {
        return response.send(user.email ? user.email : "")
    }
})