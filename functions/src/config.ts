import * as firebaseAdmin from 'firebase-admin';

const serviceAccount = require('../gigafridge-f029a-firebase-adminsdk-4p94z-b5464bcf61.json');

firebaseAdmin.initializeApp({
    credential: firebaseAdmin.credential.cert(serviceAccount),
    databaseURL: 'https://gigafridge-f029a.firebaseio.com'
});

export const admin = firebaseAdmin