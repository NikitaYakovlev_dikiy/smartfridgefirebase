import * as util from '../util'

class User extends util.BdModel {
    public imei: string = ""
    public name: string = ""
}

export class UserTable extends util.BdTable<User> {
    path: string = "/user"
    model: User = new User
}