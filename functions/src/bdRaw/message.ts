import * as util from '../util'

class Message extends util.BdModel {
    public message: string = ""
}

export class MessageTable extends util.BdTable<Message> {
    path: string = "/message"
    model: Message = new Message
    auth = util.AuthType.uidIsRaw
}