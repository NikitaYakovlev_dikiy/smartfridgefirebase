import * as util from '../util'
import * as functions from 'firebase-functions'


class Product extends util.BdIdModel {
    public baseHeft: string = ""
    public name: string = ""
}
export class ProductTable extends util.BdTable<Product> {
    path: string = "/product"
    model: Product = new Product

    updatedfun() {
        return {
            base: this.fun(),
            onUpdate: this.onUpdate()
        }
    }

    private onUpdate() {
        return functions.database
            .ref(this.path + "/items")
            .onUpdate(async (snapshot, context) => {
                if (snapshot.before === undefined) {
                    console.log("undefined");

                    return
                }
                if (snapshot.before.ref.parent === null) {
                    console.log("snapshot.before.ref.parent");
                    return
                }
            
                const countRef = snapshot.before.ref.parent.child("version")
                return countRef.transaction(count => {
                    return count + 1
                })
            })
    }
}