import * as util from '../util'

class Fridge extends util.BdModel {
    public imei: string = ""
    public name: string = ""
}

export class FridgeTable extends util.BdTable<Fridge> {
    path: string = "/fridge"
    model: Fridge = new Fridge
    auth = util.AuthType.uidIsId
}