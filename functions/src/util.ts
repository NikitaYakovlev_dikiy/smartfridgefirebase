import * as functions from 'firebase-functions'
import * as config from './config'
import * as userAuth from './auth/userAuth'


export enum AuthType {
    uidIsRaw,
    uidIsId
}
export class CountItem {

    refString: string

    constructor(ref: string) {
        this.refString = ref + "/items/{itemId}"
    }

    private increment() {
        return functions.database
            .ref(this.refString)
            .onCreate(async (snapshot, context) => {
                if (snapshot.ref.parent === null) {
                    return
                }
                if (snapshot.ref.parent.parent === null) {
                    return
                }

                const countRef = snapshot.ref.parent.parent.child('count')
                return countRef.transaction(count => {
                    return count + 1
                })
            })
    }

    private decrement() {
        return functions.database
            .ref(this.refString)
            .onDelete(async (snapshot, context) => {
                if (snapshot.ref.parent === null) {
                    return
                }
                if (snapshot.ref.parent.parent === null) {
                    return
                }
                const countRef = snapshot.ref.parent.parent.child('count')

                return countRef.transaction(count => {
                    return count - 1
                })
            })
    }
    public countItem(): Object {
        return { decrement: this.decrement(), increment: this.increment() }
    }
}
export abstract class BdModel {

    public isNotFieldEmpty(): Boolean {
        for (const key in this) {
            const valueLength = `${this[key]}`.length
            if (valueLength === 0) {
                return false
            }
        }
        return true
    }

    fromRequest(response: functions.Request) {
        Object.assign(this, response.body)
    }
}
export abstract class BdIdModel extends BdModel {

    public id: string = ""

    withoutId(): Object {
        const outObject = this
        delete outObject.id
        return outObject
    }
}
export abstract class BdTable<model extends BdModel> {

    abstract path: string
    abstract model: model
    public auth: AuthType | null = null

    public create(onComplete?: () => void, onError?: (error: Error | number) => void) {
        return functions.https.onRequest(async (request, response) => {

            let itemPath = `${this.path}/items`
            const userUid = this.getUserUid(request)

            if (this.auth !== null) {
                if (userUid !== null) {
                    itemPath += "/items/" + userAuth
                } else {
                    this.callError(response, onError, 401)
                    return
                }
            }

            const requestModel: model = Object.create(this.model)
            requestModel.fromRequest(request)

            if (requestModel.isNotFieldEmpty()) {

                const database = config.admin.database()

                if (requestModel instanceof BdIdModel || this.auth === AuthType.uidIsId) {

                    let id: any
                    let modelWithoutId: any
                    if (requestModel instanceof BdIdModel) {
                        id = requestModel.id
                        modelWithoutId = requestModel.withoutId
                    } else {
                        id = userUid
                        modelWithoutId = requestModel
                    }

                    // tslint:disable-next-line: no-floating-promises
                    database.ref(`${itemPath}/${id}`)
                        .set(modelWithoutId, (error) => {
                            if (error === null) {
                                if (onComplete !== undefined) {
                                    onComplete()
                                }
                                response.send({ status: requestModel })
                            } else {
                                this.callError(response, onError)
                            }
                        })
                } else {
                    database.ref(`${itemPath}`)
                        .push(requestModel)
                        .then((snapshot) => {
                            if (onComplete !== undefined) {
                                onComplete()
                            }
                            response.send({ status: requestModel })
                        })
                }
            } else {
                this.callError(response, onError, 400)
            }
        })
    }
    private getUserUid(request: functions.Request): String | null {
        return userAuth.getUser(request.headers.authorization)
    }

    private callError(response: functions.Response, onError?: (error: Error | number) => void, code: number = 500) {
        if (onError !== undefined) {
            onError(code)
        }
        return response.sendStatus(code)
    }

    public count(addedPath: string = ""): Object {
        return new CountItem(this.path + addedPath).countItem()
    }

    public fun(): Object {
        if (this.auth === AuthType.uidIsRaw) {
            return {
                countUser: this.count(),
                count: this.count("/items/{userUid}"),
                create: this.create(),
            }
        } else {
            return {
                count: this.count(),
                create: this.create()
            }
        }
    }

}
